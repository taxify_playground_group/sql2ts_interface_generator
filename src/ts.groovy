import com.intellij.database.model.DasTable
import com.intellij.database.util.Case
import com.intellij.database.util.DasUtil

NEWLINE = System.getProperty("line.separator")
typeMapping = [
        (~/(?i)int/)                      : "number",
        (~/(?i)float|double|decimal|real/): "number",
        (~/(?i)datetime|timestamp/)       : "Date",
        (~/(?i)date/)                     : "Date",
        (~/(?i)time/)                     : "Date",
        (~/(?i)/)                         : "string"
]


def generate(table, dir) {
    def className = javaName(table.getName(), true)
    def fields = calcFields(table)
    def buffer = "export interface ${className}TableRow {$NEWLINE"
    fields.each() {
        buffer = buffer + ("    ${it.name}: ${it.type};$NEWLINE")
    }
    buffer = buffer + "}$NEWLINE"
    CLIPBOARD.set(buffer);
}

def calcFields(table) {
    DasUtil.getColumns(table).reduce([]) { fields, col ->
        def spec = Case.LOWER.apply(col.getDataType().getSpecification())
        def typeStr = typeMapping.find { p, t -> p.matcher(spec).find() }.value
        fields += [[
                           name : col.getName(),
                           type : typeStr]]
    }
}

def javaName(str, capitalize) {
    def s = com.intellij.psi.codeStyle.NameUtil.splitNameIntoWords(str)
            .collect { Case.LOWER.apply(it).capitalize() }
            .join("")
            .replaceAll(/[^\p{javaJavaIdentifierPart}[_]]/, "_")
    capitalize || s.length() == 1? s : Case.LOWER.apply(s[0]) + s[1..-1]
}



SELECTION.filter { it instanceof DasTable }.each { generate(it, null) }
